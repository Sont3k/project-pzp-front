export const iconSize = "big";

export const signInPageInfo = {
  pageName: "Авторизація",
  loginText: "Логін",
  passwordText: "Пароль",
  signInText: "Увійти",
  signUpText: "Реєстрація"
};

export const signUpPageInfo = {
  pageName: "Реєстрація",
  loginText: "Логін",
  initialsText: "ПІБ",
  emailText: "E-mail",
  passwordText: "Пароль",
  repeatPasswordText: "Повторіть пароль",
  signUpText: "Реєстрація"
};
