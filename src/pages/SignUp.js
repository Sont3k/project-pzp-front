import React, { useState } from "react";

import Header from "../components/Header";
import { Icon, Input } from "semantic-ui-react";
import Button from "../components/SignButton";
import { signUpPageInfo } from "../constants/Constants";
import signUpStyles from '../styles/SignUp.module.css';
import ReCAPTCHA from "react-google-recaptcha";

function SignUp(props) {
  const [isPasswordVisible, setIsPasswordVisible] = useState(false);
  const [isRepeatPasswordVisible, setIsRepeatPasswordVisible] = useState(false);

  const togglePasswordVisibility = () => {
    setIsPasswordVisible(!isPasswordVisible);
  };

  const toggleRepeatPasswordVisibility = () => {
    setIsRepeatPasswordVisible(!isRepeatPasswordVisible);
  };

  return (
    <div className={signUpStyles.container}>
      <Header />
      <main className={signUpStyles.main}>
        <span className={signUpStyles.pageName}>{signUpPageInfo.pageName}</span>
        <div className={signUpStyles.inputFields}>
          <div className={signUpStyles.inputField}>
            <Input
              size="big"
              focus
              transparent
              placeholder={signUpPageInfo.loginText}
            />
            <div className={signUpStyles.underline}></div>
          </div>

          <div className={signUpStyles.spacer} />

          <div className={signUpStyles.inputField}>
            <Input
              size="big"
              focus
              transparent
              placeholder={signUpPageInfo.initialsText}
            />
            <div className={signUpStyles.underline}></div>
          </div>

          <div className={signUpStyles.spacer} />

          <div className={signUpStyles.inputField}>
            <Input
              size="big"
              focus
              transparent
              type="email"
              placeholder={signUpPageInfo.emailText}
            />
            <div className={signUpStyles.underline}></div>
          </div>

          <div className={signUpStyles.spacer} />

          <div className={signUpStyles.inputField}>
            <Input
              size="big"
              focus
              transparent
              placeholder={signUpPageInfo.passwordText}
              type={isPasswordVisible ? "string" : "password"}
              icon={
                <div onClick={() => togglePasswordVisibility()}>
                  <Icon color="blue" name={isPasswordVisible ? "eye slash" : "eye"} />
                </div>
              }
            />
            <div className={signUpStyles.underline}></div>
          </div>

          <div className={signUpStyles.spacer} />

          <div className={signUpStyles.inputField}>
            <Input
              size="big"
              focus
              transparent
              placeholder={signUpPageInfo.repeatPasswordText}
              type={isRepeatPasswordVisible ? "string" : "password"}
              icon={
                <div onClick={() => toggleRepeatPasswordVisibility()}>
                  <Icon color="blue" name={isRepeatPasswordVisible ? "eye slash" : "eye"} />
                </div>
              }
            />
            <div className={signUpStyles.underline}></div>
          </div>

          <div className={signUpStyles.spacer} />

          <ReCAPTCHA
            className={signUpStyles.reCaptcha}
            size="compact"
            sitekey="Your client site key"
          />
        </div>

        <div className={signUpStyles.buttons}>
          <Button type="SignIn" text={signUpPageInfo.signUpText} />
        </div>
      </main>
      <footer className={signUpStyles.footer}></footer>
    </div>
  );
}

export default SignUp;
