import React, { useState } from "react";
import { Redirect } from "react-router-dom";

import Header from "../components/Header";
import { Icon, Input } from "semantic-ui-react";
import SignButton from "../components/SignButton";
import { signInPageInfo } from "../constants/Constants";
import signInStyles from "../styles/SignIn.module.css";
import ReCAPTCHA from "react-google-recaptcha";

//TODO make blue input text and placeholder
//TODO make password with * instead dots
//! "size=big" can cause asymmetry
//TODO redirect to "/register" not working

//TODO add input data saving
//TODO add input validation

//TODO (not high priority) register recaptcha

function SignIn(props) {
  const [isPasswordVisible, setIsPasswordVisible] = useState(false);

  const togglePasswordVisibility = () => {
    setIsPasswordVisible(!isPasswordVisible);
  };

  const signUpOnClick = () => {
    console.log("Clicked");
    return <Redirect to="/register" />;
  };

  return (
    <div className={signInStyles.container}>
      <Header />
      <main className={signInStyles.main}>
        <span className={signInStyles.pageName}>{signInPageInfo.pageName}</span>
        <div className={signInStyles.inputFields}>
          <div className={signInStyles.inputField}>
            <Input
              size="big"
              focus
              transparent
              placeholder={signInPageInfo.loginText}
            />
            <div className={signInStyles.underline}></div>
          </div>

          <div className={signInStyles.spacer} />

          <div className={signInStyles.inputField}>
            <Input
              size="big"
              focus
              transparent
              placeholder={signInPageInfo.passwordText}
              type={isPasswordVisible ? "string" : "password"}
              icon={
                <div onClick={() => togglePasswordVisibility()}>
                  <Icon
                    color="blue"
                    name={isPasswordVisible ? "eye slash" : "eye"}
                  />
                </div>
              }
            />
            <div className={signInStyles.underline}></div>
          </div>

          <div className={signInStyles.spacer} />

          <ReCAPTCHA
            className={signInStyles.reCaptcha}
            size="compact"
            sitekey="Your client site key"
          />
        </div>

        <div className={signInStyles.buttons}>
          <SignButton type="SignIn" text={signInPageInfo.signInText} />
          <SignButton
            type="SignUp"
            text={signInPageInfo.signUpText}
            onClick={() => signUpOnClick()}
          />
        </div>
      </main>
      <footer className={signInStyles.footer}></footer>
    </div>
  );
}

export default SignIn;
