import React from "react";
import buttonStyles from '../styles/Button.module.css';

export default function SignButton(props) {
  const Button = () => {
    if (props.type === "SignIn") {
      return (
        <div className={buttonStyles.signInButton} onClick={props.onClick}>
          <span className={buttonStyles.signInButtonText}>{props.text}</span>
        </div>
      );
    } else if (props.type === "SignUp") {
      return (
        <div className={buttonStyles.signUpButton} onClick={props.onClick}>
          <span className={buttonStyles.signUpButtonText}>{props.text}</span>
        </div>
      );
    }
  };

  return <Button />;
}
