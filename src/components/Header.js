import React, { useState } from "react";

import { Sidebar, Icon } from "semantic-ui-react";
import { iconSize } from "../constants/Constants";
import headerStyles from '../styles/Header.module.css';

function Header(props) {
  const [isSidebarVisible, setIsSidebarVisible] = useState(false);

  const toggleSidebar = () => {
    setIsSidebarVisible(!isSidebarVisible);
  };

  return (
    <header className={headerStyles.header}>
      <span className={headerStyles.logo}>
        p<span>Z</span>p
      </span>

      <div className={headerStyles.sidebarButton} onClick={() => toggleSidebar()}>
        <Icon className={headerStyles.sidebarIcon} name="bars" size={iconSize} />
        <Sidebar visible={isSidebarVisible} />
      </div>
    </header>
  );
}

export default Header;
